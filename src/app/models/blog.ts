export interface IBlogPost {
        id: number,
        userId: number,
        title: string,
        body: string
}

export interface IBlogComment {
        id: number,
        postId: number,
        name: string,
        email: string,
        body: string
}

import { Injectable } from '@angular/core'
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

/* Models */
import { IBlogPost, IBlogComment } from "../models/blog"

@Injectable({
    providedIn: 'root'
})

export class BlogService {
        // it should put in enviroment 
        private apiUrl = 'https://jsonplaceholder.typicode.com'
        private apiPostEndpoint = '/posts'
        private apiCommentEndpoint = `/comments`

        constructor(private http: HttpClient) { }

        /* Public API methods */ 
        getPosts():Observable<IBlogPost[]> {
            return this.http.get<IBlogPost[]>(`${this.apiUrl }${this.apiPostEndpoint}`).pipe(
                catchError(this.handleError)
            )
        }
        
        getCommentByPostId(postId:number):Observable<IBlogComment[]>{
            return this.http.get<IBlogComment[]>(`${this.apiUrl}${this.apiPostEndpoint}/${postId}${this.apiCommentEndpoint}`).pipe(
                catchError(this.handleError)
            )
        }

        postPost(postData: IBlogPost):Observable<IBlogPost> {
            return this.http.post<any>(`${this.apiUrl }${this.apiPostEndpoint}`,postData).pipe(
                catchError(this.handleError)
            )
        }

        /* Error handling */
        handleError(errorResponse: HttpErrorResponse) {
            let errorMessage = ''
            if (errorResponse.error instanceof ErrorEvent) {
                errorMessage = `Server response error ${errorResponse.error.message}`
            } else {
                errorMessage = `Server returned code: ${errorResponse.status}, message: ${errorResponse.message}`
            }
            return  throwError(errorMessage)
        }
}
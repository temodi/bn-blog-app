import { Component, Input, OnInit } from '@angular/core';
import { IBlogComment } from 'src/app/models/blog';
import { BlogService } from 'src/app/services/blog.service';

@Component({
  selector: 'blog-post-comment',
  templateUrl: './post-comment.component.html',
  styleUrls: ['./post-comment.component.css']
})

export class PostCommentComponent implements OnInit {

  @Input() postId: number;
  comments:IBlogComment[];
  errorMessage: string = '';

  constructor(
      private blogService: BlogService
  ) { }

  ngOnInit():void { 
      this.blogService.getCommentByPostId(this.postId).subscribe({
          next: comments => this.comments = comments,
          error: error => this.errorMessage = error,
      })
  }
}

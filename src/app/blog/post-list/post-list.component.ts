import { Component, OnInit} from '@angular/core';
import { IBlogPost } from '../../models/blog';
import { BlogService } from '../../services/blog.service';

@Component({
  selector: 'blog-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})

export class PostListComponent implements OnInit {

  contentTitle: string  = 'Blog bejegyzések';
  posts: IBlogPost[]  = [];
  errorMessage: string = '';

  private visibleComments:boolean[] = [];

  constructor(
      private  blogService: BlogService
  ) {  }

  ngOnInit():void { 
          this.blogService.getPosts().subscribe({
              next: posts => this.posts = posts,
              error: error => this.errorMessage = error
          })
  }

  toggleComments(postId):void {
      if (this.visibleComments[postId] === undefined) {
            this.visibleComments[postId] = false;
      } 
      this.visibleComments[postId] = !this.visibleComments[postId];
  }
}

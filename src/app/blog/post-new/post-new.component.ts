import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { IBlogPost } from '../../models/blog';
import { BlogService } from '../../services/blog.service';

@Component({
    selector: 'blog-post-new',
    templateUrl: './post-new.component.html',
    styleUrls: ['./post-new.component.css']
})

export class PostNewComponent implements OnInit {

    contentTitle: string = 'Új blog bejegyzés';
    postForm: FormGroup;
    post: IBlogPost;
    errorMessage: string = ''; 
    formSubmitSuccess: boolean;

    constructor(
        private formBuilder: FormBuilder,
        private blogService: BlogService,
    ) { 
        this.formSubmitSuccess = false;
    }

    ngOnInit() {
        this.postForm = this.formBuilder.group({
                name: [null, Validators.required],
                email:  [null, [Validators.required, Validators.email]],
                title: [null, Validators.required],
                body: [null, Validators.required]
        })
     }

    onSubmit() {
        if (this.postForm.valid) {
            this.blogService.postPost(this.postForm.value).subscribe({
                next: post => this.successSubmit(post),
                error: error => this.errorMessage = error
            })
        } else {
            this.vaildateFormData(this.postForm);
        }
    }
    successSubmit(post:IBlogPost): void {
        // We can use post for user inform, if you want 
        this.formSubmitSuccess = true
        this.resetForm()
    } 
    resetForm():void {
        this.postForm.reset()
    }
    vaildateFormData(formGroup: FormGroup) { 
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
              control.markAsTouched({ onlySelf: true }); 
            } else if (control instanceof FormGroup) {
              this.vaildateFormData(control);
            }
          });
    }
    
    isFieldValidCSS(field: string):string {
        if (this.postForm.get(field).valid) {
            return 'is-valid'
        } else if (this.postForm.get(field).touched) {
            return 'is-invalid'
        }
        return ''
     }
}
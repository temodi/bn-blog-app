import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/* Components */
import { BlogComponent } from './blog.component';
import { PostListComponent } from './post-list/post-list.component';
import { PostCommentComponent } from './post-comment/post-comment.component';
import { PostNewComponent } from './post-new/post-new.component';

 
@NgModule({
  declarations: [
    BlogComponent,
    PostListComponent,
    PostCommentComponent,
    PostNewComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    BlogComponent
  ],
  providers: []
})
export class BlogModule { }
